package com.hepta.agenda.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.hepta.agenda.entity.Usuario;
import com.hepta.agenda.persistence.SuperDAO;

@Path("/usuario")
public class RESTUsuario {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/cadastro")
	public Response cadastro(Usuario user) {
		SuperDAO dao = new SuperDAO(Usuario.class);
		System.out.println("aquii");

		try {
			dao.save(user);
			return Response.ok().entity(user).build();
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(500).entity("Erro ao cadastrar!").build();
		}
	}
}
