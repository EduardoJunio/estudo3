package com.hepta.agenda.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.validation.Valid;

public class SuperDAO <T, I extends Serializable> {

	private Class<T> persistedClass;

	public SuperDAO() {

	}

	public SuperDAO(Class<T> persistedClass) {
		this();
		this.persistedClass = persistedClass;
	}

	public T save(@Valid T entity) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return entity;
	}

	public T update(@Valid T newEntity) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		T entity = null;
		try {
			em.getTransaction().begin();
			entity = em.merge(newEntity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return entity;
	}

	public void delete(I id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		try {
			em.getTransaction().begin();
			T entity = em.find(persistedClass, id);
			T mergedEntity = em.merge(entity);
			em.remove(mergedEntity);
			em.getTransaction().commit();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}

	}

	public T find(I id) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		T obj = null;
		try {
			obj = em.find(persistedClass, id);
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return obj;
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> getAll() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<T> list = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM " + persistedClass.getName());
			list = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return list;
	}

	@SuppressWarnings({ "unchecked", "hiding" })
	public <T> List<T> getAllPaginated(Integer initialPage, Integer maxValue) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		List<T> list = new ArrayList<>();

		try {
			StringBuilder sql = new StringBuilder("FROM ");
			sql.append(persistedClass.getName());
			Query query = em.createQuery(sql.toString());
			query.setFirstResult(initialPage);
			query.setMaxResults(maxValue);
			list = query.getResultList();
		} catch (Exception e) {
			em.getTransaction().rollback();
			throw new Exception(e);
		} finally {
			em.close();
		}
		return list;
	}
	
}
